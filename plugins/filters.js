import Vue from "vue";
import { Icon } from "@iconify/vue2";

export default () => {
  Vue.mixin({
    components: {
      Icon
    },
    filters: {
      numberFormat(num) {
        return new Intl.NumberFormat("vi", {}).format(Number(num));
      },
    },
    computed: {
      mobile() {
        return this.$vuetify.breakpoint.mobile;
      },
    },
  });
};
