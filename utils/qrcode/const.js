export const SHAPE_STYLE = Object.freeze({
  DEFAULT: 0,
  DOTS: 1,
  ROUND_1: 2,
  ROUND_2: 3,
  BRICK: 4,
});
export const LOGO_STYLE = Object.freeze({
  NONE: 0,
  UPLOAD: 1,
});

export const EDGES_STYLE = Object.freeze({
  DEFAULT: 0,
  ROUND: 1,
  DOTS: 2,
});
export const EDGE_TYPE = Object.freeze({
  TOP_LEFT: 0,
  TOP_RIGHT: 1,
  BOTTOM_LEFT: 2,
});
