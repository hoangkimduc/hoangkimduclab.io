const QRCode = require("qrcode-svg");
import { Context, Element } from "svgcanvas";
import { SHAPE_STYLE, EDGE_TYPE } from "./const";

const LARGE_DOT_AREA = 7;
const logoAreaPercent = 25;

const IconContentTemplate = [
  [true, true, true, false, true],
  [false, true, true, false, true],
  [true, true, false, true, true],
  [false, true, true, true, false],
  [true, false, true, true, true]
];
const IconEdgeTemplate = [
  [true, true, true, true, true, true, true],
  [true, false, false, false, false, false, true],
  [true, false, true, true, true, false, true],
  [true, false, true, true, true, false, true],
  [true, false, true, true, true, false, true],
  [true, false, false, false, false, false, true],
  [true, true, true, true, true, true, true]
];

export class KDQRCode {
  constructor(width = 300, height = 300, canvas, padding = 0) {
    this.canvas = canvas;
    this.text = " ";
    this.logoAble = false;
    this.logo = new Image();
    this.padding = padding;

    this.context = null;
    this.box = {};
    this.shapeStyle = 0;
    this.content = {
      value: [],
      modules: [],
      color: "#000000FF"
    };

    this.edges = {
      topLeft: {
        id: 0,
        value: [],
        color: "#000000FF",
        innerColor: "#000000FF",
        type: SHAPE_STYLE.DEFAULT,
        title: "Top Left"
      },
      topRight: {
        id: 1,
        value: [],
        color: "#000000FF",
        innerColor: "#000000FF",
        type: SHAPE_STYLE.DEFAULT,
        title: "Top Right"
      },
      bottomLeft: {
        id: 2,
        value: [],
        color: "#000000FF",
        innerColor: "#000000FF",

        type: SHAPE_STYLE.DEFAULT,
        title: "Bottom Left"
      },
      all: {
        id: 3,
        value: [],
        color: "#000000FF",
        innerColor: "#000000FF",
        type: SHAPE_STYLE.DEFAULT,
        title: "All"
      }
    };
    this.width = width;
    this.height = height;
    this.backgroundColor = "#FFFFFFFF";
    this.qrcodeRect = {
      x: 0,
      y: 0,
      width: 0
    }
    this.allowGradient = false
    this.gradient = {
      stops: [
        ' <stop offset="0" stop-color="#000000FF" />',
        ' <stop offset="1" stop-color="#9C0000FF" />'
      ],
      angleCoords: { x1: "50%", y1: "100%", x2: "50%", y2: "0%" }
    };

    // 
    this.viewSize = 0;
  }

  reset() {
    this.content.value = [];
    this.content.modules = [];

    this.edges.topLeft.value = [];
    this.edges.topRight.value = [];
    this.edges.bottomLeft.value = [];
  }

  renderQRCodeContent(text) {
    if (text) this.text = text;
    var qrcodeObj = new QRCode({
      content: this.text,
      join: true,
      ecl: this.logoAble ? "Q" : "M"
    });

    this.reset();
    var modules = qrcodeObj.qrcode.modules;
    const paddingVertical = Array.from(Array(this.padding)).map(item =>
      Array.from(Array(modules.length + this.padding * 2)).map(item => false)
    );
    const paddingHorizontal = Array.from(Array(this.padding)).map(
      item => false
    );
    this.content.modules = [
      ...paddingVertical,
      ...modules.map(item => {
        return [...paddingHorizontal, ...item, ...paddingHorizontal];
      }),
      ...paddingVertical
    ];
    //

    var length = this.content.modules.length;
    const context = this.setupCanvas(length);
    this.context = context;

    this.box = {
      width: Math.floor(this.width / length),
      height: Math.floor(this.height / length)
    };

    this.qrcodeRect = {
      x: this.box.width * this.padding,
      y: this.box.width * this.padding,
      width: this.box.width * (length - this.padding * 2)
    };

    // console.log("w=", this.width);
    // console.log("drawW=", length * this.box.width);

    // this.canvas.width = this.box.width * length;
    // this.canvas.height = this.box.height * length;
    // this.canvas.width = this.box.width * length;
    // this.canvas.height = this.box.height * length;
    // this.width = this.canvas.width;
    // this.height = this.canvas.height;

    for (var y = 0; y < length; y++) {
      for (var x = 0; x < length; x++) {
        //
        const { status: edgeStatus, type: edgeType } = this.isEdges(x, y);
        if (edgeStatus) {
          switch (edgeType) {
            case EDGE_TYPE.TOP_LEFT:
              this.edges.topLeft.value.push([x, y]);
              break;
            case EDGE_TYPE.TOP_RIGHT:
              this.edges.topRight.value.push([x, y]);
              break;
            case EDGE_TYPE.BOTTOM_LEFT:
              this.edges.bottomLeft.value.push([x, y]);
              break;
            default:
              break;
          }
        } else {
          this.content.value.push([x, y]);
        }

        if (this.logoAble && this.isLogoArea(x, y)) {
          this.content.modules[x][y] = false;
        }
      }
    }
  }

  isEdges(x, y) {
    const length = this.content.modules.length;
    const _options = {
      status: false,
      type: null
    };
    const areaHeight = LARGE_DOT_AREA + this.padding;
    if (x >= this.padding && x < length - this.padding) {
      switch (true) {
        case y >= this.padding && y <= areaHeight - 1:
          // TOP
          if (x <= areaHeight) {
            _options.status = true;
            _options.type = EDGE_TYPE.TOP_LEFT;
          } else if (x >= length - areaHeight) {
            _options.status = true;
            _options.type = EDGE_TYPE.TOP_RIGHT;
          }
          break;
        case y < length - this.padding && y >= length - areaHeight:
          if (x <= areaHeight) {
            _options.status = true;
            _options.type = EDGE_TYPE.BOTTOM_LEFT;
          }
          break;
      }
    }
    return _options;
  }
  isEdgesInner(x, y) {
    const length = this.content.modules.length;
    const _options = {
      status: false,
      type: null
    };
    const padding = this.padding + 2;

    const areaHeight = 3;

    if (x >= padding && x < length - padding) {
      if (y >= padding && y <= padding + areaHeight) {
        if (x < padding + areaHeight) {
          _options.status = true;
          _options.type = EDGE_TYPE.TOP_LEFT;
        } else if (x >= length - (padding + areaHeight)) {
          _options.status = true;
          _options.type = EDGE_TYPE.TOP_RIGHT;
        }
      } else if (y >= length - areaHeight - padding && y <= length - padding) {
        if (x < padding + areaHeight) {
          _options.status = true;
          _options.type = EDGE_TYPE.BOTTOM_LEFT;
        }
      }
    }

    return _options;
  }

  isLogoArea(x, y) {
    const {
      content: { modules }
    } = this;

    const dotsRemove = (logoAreaPercent * modules.length) / 100;

    const centerPosition = Math.floor(modules.length / 2);
    const halfPartDotsRemove = Math.floor(dotsRemove / 2);
    const whiteArea = [
      centerPosition - halfPartDotsRemove,
      centerPosition + halfPartDotsRemove
    ];
    if (
      x >= whiteArea[0] &&
      x <= whiteArea[1] &&
      y >= whiteArea[0] &&
      y <= whiteArea[1]
    ) {
      return true;
    }
    return false;
  }

  drawRect(x, y) {
    const { box, context } = this;

    context.rect(x * box.width, y * box.height, box.width, box.height);
  }
  drawRound(x, y, sides) {
    // context.rect(x * box.width, y * box.height, box.width, box.height);
    const { box, context } = this;
    const radius = box.width / 2;
    const _x = x * box.width;
    const _y = y * box.height;
    this.roundRect(context, _x, _y, box.width, box.height, sides, true, false);
  }
  drawDot(x, y) {
    const { box, context } = this;
    const radius = box.width / 2;
    const _x = x * box.width + box.width / 2;
    const _y = y * box.height + box.height / 2;
    context.arc(_x, _y, radius, 0, Math.PI * 2);
  }
  drawTriangle(_x, _y, size, position = { tl: 0, tr: 0, br: 0, bl: 0 }) {
    const { box, context } = this;
    
    const x = _x * box.width;
    const y = _y * box.height;


    context.beginPath();
    switch (true) {
      case position.bl:
        context.moveTo(x, y + size);
        context.quadraticCurveTo(
          x ,
          y + box.width,
          x + size,
          y + size * 2
        );
        context.lineTo(x, y + box.width);
        break;
      case position.br:
        context.moveTo(x + box.width, y + size);
        context.quadraticCurveTo(
          x + box.width,
          y + box.width,
          x + box.width - size,
          y + size * 2
        );
        context.lineTo(x + box.width, y + box.width);
        break;
      case position.tl:
        context.moveTo(x, y);
        context.lineTo(x + size, y);

        context.quadraticCurveTo(x, y, x, y + size);
        break;
      case position.tr:

        context.moveTo(x + size, y);
        context.quadraticCurveTo(x + box.width, y, x + box.width, y + size);
        context.lineTo(x + box.width, y);


        break;

      default:
        break;
    }
    context.closePath();
    context.fill();
  }

  //
  drawEdge(edgeType) {
    const {
      context,
      content: { modules },
      edges
    } = this;
    let _edgeType = EDGE_TYPE.TOP_LEFT;
    switch (edgeType) {
      case EDGE_TYPE.TOP_LEFT:
        _edgeType = "topLeft";
        break;
      case EDGE_TYPE.TOP_RIGHT:
        _edgeType = "topRight";
        break;
      case EDGE_TYPE.BOTTOM_LEFT:
        _edgeType = "bottomLeft";
        break;
      default:
        _edgeType = EDGE_TYPE.TOP_LEFT;
        break;
    }

    const _color = edges[_edgeType].color;
    const _innerColor = edges[_edgeType].innerColor;

    for (const [x, y] of edges[_edgeType].value) {
      this.drawPointEdge(x, y, _color, _innerColor);
    }
  }
  drawPointEdge(x, y, _color, _innerColor) {
    const {
      context,
      content: { modules }
    } = this;
    const color = _color || "#000";
    const innerColor = _innerColor || "#000";

    const module = modules[x][y];
    if (module) {
      if (this.isEdgesInner(x, y).status) {
        this.drawPoint(x, y, innerColor);
      } else {
        this.drawPoint(x, y, color);
      }
    } else {
      this.drawPoint(x, y, color);
    }
  }
  // drawPointEdge(x, y, _color, _innerColor) {
  //   const {
  //     context,
  //     content: { modules }
  //   } = this;
  //   const color = _color || "#000";
  //   const innerColor = _innerColor || "#000";

  //   context.beginPath();
  //   this.drawRect(x, y);
  //   const module = modules[x][y];
  //   if (module) {
  //     if (this.isEdgesInner(x, y).status) {
  //       context.fillStyle = innerColor;
  //     } else {
  //       context.fillStyle = color;
  //     }
  //   } else {
  //     context.fillStyle = this.backgroundColor;
  //   }
  //   context.fill();
  // }
  drawPoint(x, y, colorStyle) {
    const {
      context,
      content: { modules, color: _color },
      box
    } = this;

    var color = colorStyle || _color;

    const length = modules.length;

    const module = modules[x][y];

    context.beginPath();

    const roundRadius = box.width / 1.8;
    const triangleSize = box.width / 2;
    const lineWidth = this.width / this.viewSize;


    // relation dots
    const _leftBox = x - 1;
    const _rightBox = x + 1;
    const dL = _leftBox < 0 ? false : modules[_leftBox][y];
    const dR = _rightBox > length - 1 ? false : modules[_rightBox][y];
    let dT = false;
    let dB = false;
    // angle
    let aTL = false;
    let aTR = false;
    let aBL = false;
    let aBR = false;

    switch (true) {
      case y === 0:
        dT = false;
        dB = modules[x][y + 1];
        //
        aTL = false;
        aTR = false;
        aBL = _leftBox < 0 ? false : modules[_leftBox][y + 1];
        aBR = _rightBox > length - 1 ? false : modules[_rightBox][y + 1];
        break;
      case y === length - 1:
        dT = modules[x][y - 1];
        dB = false;
        //
        aBL = false;
        aBR = false;
        aTL = _leftBox < 0 ? false : modules[_leftBox][y - 1];
        aTR = _rightBox > length - 1 ? false : modules[_rightBox][y - 1];
        break;
      default:
        dT = modules[x][y - 1];
        dB = modules[x][y + 1];
        //
        aBL = _leftBox < 0 ? false : modules[_leftBox][y + 1];
        aBR = _rightBox > length - 1 ? false : modules[_rightBox][y + 1];
        aTL = _leftBox < 0 ? false : modules[_leftBox][y - 1];
        aTR = _rightBox > length - 1 ? false : modules[_rightBox][y - 1];
        break;
    }
    // end relation dots

    switch (this.shapeStyle) {
      case SHAPE_STYLE.ROUND_2:
      case SHAPE_STYLE.ROUND_1:
        if (module) {
          switch (true) {
            case !dT && !dB && !dL && !dR:
              // cricle
              this.drawDot(x, y);
              break;
            case dT + dB + dL + dR === 1: {
              // round a side
              switch (true) {
                case dT:
                  this.drawRound(x, y, {
                    bl: roundRadius,
                    br: roundRadius
                  });
                  break;
                case dB:
                  this.drawRound(x, y, {
                    tl: roundRadius,
                    tr: roundRadius
                  });
                  break;
                case dL:
                  this.drawRound(x, y, {
                    tr: roundRadius,
                    br: roundRadius
                  });
                  break;
                case dR:
                  this.drawRound(x, y, {
                    tl: roundRadius,
                    bl: roundRadius
                  });
                  break;
              }
              break;
            }
            case dT + dB + dL + dR === 2 && dT + dB === 1 && dL + dR === 1: {
              // round to a angle
              switch (true) {
                case dT && dR:
                  this.drawRound(x, y, {
                    bl: roundRadius
                  });
                  break;
                case dT && dL:
                  this.drawRound(x, y, {
                    br: roundRadius
                  });
                  break;
                case dB && dL:
                  this.drawRound(x, y, {
                    tr: roundRadius
                  });
                  break;
                case dB && dR:
                  this.drawRound(x, y, {
                    tl: roundRadius
                  });
                  break;

                default:
                  break;
              }
              break;
            }
            default:
              // this.drawRound(x, y);
              this.drawRect(x, y);

              break;
          }
        } else {
          this.drawRect(x, y);
        }
        this.fill(module ? color : this.backgroundColor);
        //
        if (this.shapeStyle === SHAPE_STYLE.ROUND_2) {
          // curve angle
          if (!module) {
            if (aBL && dL && dB) {
              context.beginPath();
              this.drawTriangle(x, y, triangleSize, {
                bl: true
              });
              this.fill(color);
              context.beginPath();
              this.drawDot(x, y);
              this.fill(this.backgroundColor);
            }
            if (aBR && dR && dB) {
              context.beginPath();
              this.drawTriangle(x, y, triangleSize, {
                br: true
              });
              this.fill(color);
              context.beginPath();
              this.drawDot(x, y);
              this.fill(this.backgroundColor);
            }
            if (aTL && dL && dT) {
              context.beginPath();
              this.drawTriangle(x, y, triangleSize, {
                tl: true
              });
              this.fill(color);
              context.beginPath();
              this.drawDot(x, y);
              this.fill(this.backgroundColor);
            }
            if (aTR && dR && dT) {
              context.beginPath();
              this.drawTriangle(x, y, triangleSize, {
                tr: true
              });
              this.fill(color);
              context.beginPath();
              this.drawDot(x, y);
              this.fill(this.backgroundColor);
            }
          }
        }
        break;

      case SHAPE_STYLE.DEFAULT:
        this.drawRect(x, y);
        this.fill(module ? color : this.backgroundColor);
        break;
      case SHAPE_STYLE.BRICK:
        this.drawRect(x, y);
        this.fill(module ? color : this.backgroundColor);
        if (module) {
          context.beginPath();
          context.strokeStyle = this.backgroundColor;
          context.lineWidth = lineWidth;
          this.drawRect(x, y);
          context.stroke();
        }

        break;
      case SHAPE_STYLE.DOTS:
        this.drawDot(x, y);
        this.fill(module ? color : this.backgroundColor);
        break;
    }
  }
  drawShapeContent() {
    const {
      content: { value }
    } = this;
    for (const [x, y] of value) {
      this.drawPoint(x, y);
    }
  }
  drawLogo() {
    return new Promise(async (resolve, _) => {
      this.blobToBase64(this.logo.src).then(imgData => {
        const { box, context, width, height } = this;

        var ratio = this.logo.width / this.logo.height;

        const imageWidth = ((logoAreaPercent * width) / 100) - this.box.width * 2;
        const imageHeight = imageWidth / ratio;

        const imageStartX = width / 2 - imageWidth / 2;
        const imageStartY = height / 2 - imageHeight / 2;

        var image = new Image();
        image.onload = function() {
          context.drawImage(
            this,
            imageStartX,
            imageStartY,
            imageWidth,
            imageHeight
          );
          resolve();
        };
        image.src = imgData;
      });
    });
  }

  blobToBase64(url) {
    return new Promise(async (resolve, _) => {
      const response = await fetch(url);
      const blob = await response.blob();
      const fileReader = new FileReader();
      fileReader.readAsDataURL(blob);

      fileReader.onloadend = function() {
        resolve(fileReader.result);
      };
    });
  }
  fill(color) {
    const { context } = this;

    context.fillStyle = color;
    context.fill();
  }
  async drawer() {
    const {
      context,
      content: { modules }
    } = this;

    context.fillStyle = this.backgroundColor;
    context.fillRect(0, 0, this.width, this.height);

    this.drawEdge(EDGE_TYPE.TOP_LEFT);
    this.drawEdge(EDGE_TYPE.TOP_RIGHT);
    this.drawEdge(EDGE_TYPE.BOTTOM_LEFT);

    this.drawShapeContent();

    if (this.logoAble) await this.drawLogo();
  }

  drawerIconContent() {
    const {
      context,
      content: { modules }
    } = this;

    this.content.modules = IconContentTemplate;

    var length = this.content.modules.length;

    this.content.value = Object.keys(Array.from(Array(length)))
      .map(y => {
        return Object.keys(Array.from(Array(length))).map(x => [
          Number(x),
          Number(y)
        ]);
      })
      .flat();

    this.box = {
      width: Math.floor(this.width / length),
      height: Math.floor(this.height / length)
    };
    // this.canvas.width = this.box.width * length;
    // this.canvas.height = this.box.height * length;

    context.fillStyle = this.backgroundColor;
    context.fillRect(0, 0, this.width, this.height);
    this.drawShapeContent();
  }

  /**
   * for EdgesStyle Component
   */
  drawerEdgeContent(color, innerColor) {
    const {
      context,
      content: { modules }
    } = this;

    this.content.modules = IconEdgeTemplate;

    var length = this.content.modules.length;

    this.content.value = Object.keys(Array.from(Array(length)))
      .map(y => {
        return Object.keys(Array.from(Array(length))).map(x => [
          Number(x),
          Number(y)
        ]);
      })
      .flat();

    this.box = {
      width: Math.floor(this.width / length),
      height: Math.floor(this.height / length)
    };
    this.canvas.width = this.box.width * length;
    this.canvas.height = this.box.height * length;

    context.fillStyle = this.backgroundColor;
    context.fillRect(0, 0, this.width, this.height);
    const {
      content: { value }
    } = this;
    for (const [x, y] of value) {
      this.drawPointEdge(x, y, color, innerColor);
    }
  }

  setupCanvas(length) {
    const boxWidth = Math.floor(this.width / length);

    const suggetWidth = boxWidth * length;
    this.width = suggetWidth;
    this.height = suggetWidth;
    // var dpr = window.devicePixelRatio || 1;
    // var rect = canvas.getBoundingClientRect();
    // canvas.width = this.width;
    // canvas.height = this.height;

    var context2D = this.canvas.getContext("2d", { alpha: true });

    const options = {
      height: this.width, // falsy values get converted to 500
      width: this.height, // falsy values get converted to 500
      ctx: context2D, // existing Context2D to wrap around
      enableMirroring: true // whether canvas mirroring (get image data) is enabled (defaults to false)
      // document: undefined // overrides default document object
    };

    // Creates a mock canvas context (mocks `context2D` above)
    const ctx = new Context(options);

    return ctx;
  }

  roundRect(ctx, x, y, width, height, radius, fill, stroke) {
    if (typeof stroke === "undefined") {
      stroke = true;
    }
    if (typeof radius === "undefined") {
      radius = 5;
    }
    if (typeof radius === "number") {
      radius = { tl: radius, tr: radius, br: radius, bl: radius };
    } else {
      var defaultRadius = { tl: 0, tr: 0, br: 0, bl: 0 };
      for (var side in defaultRadius) {
        radius[side] = radius[side] || defaultRadius[side];
      }
    }
    ctx.beginPath();
    ctx.moveTo(x + radius.tl, y);
    ctx.lineTo(x + width - radius.tr, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
    ctx.lineTo(x + width, y + height - radius.br);
    ctx.quadraticCurveTo(
      x + width,
      y + height,
      x + width - radius.br,
      y + height
    );
    ctx.lineTo(x + radius.bl, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
    ctx.lineTo(x, y + radius.tl);
    ctx.quadraticCurveTo(x, y, x + radius.tl, y);
    ctx.closePath();
    if (fill) {
      ctx.fill();
    }
    if (stroke) {
      ctx.stroke();
    }
  }

  getSVGURL() {
    if (process.browser) {
      const blob = new Blob([this.context.getSerializedSvg()], {
        type: "image/svg+xml"
      });
      return URL.createObjectURL(blob);
    }
  }
}
