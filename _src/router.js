import Vue from "vue";
import Router from "vue-router";
import Profile from "./views/Profile";
import Watch from "./views/Watch";
import ComponentFunctional from "./views/ComponentFunctional";
import ComponentRender from "./views/ComponentRender";
import DateRangePicker from "./views/DateRangePicker";
import Console from "./views/Console";
import Chat from "./views/Chat";
import ProgressBar from "./views/ProgressBarPage";
import DColorPickerPage from "./views/DColorPickerPage";
import Default from "./layouts/default"; 

// import Home from "./views/Home.vue";
import Home from "./views/home/index_new.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      component: Default,
      children: [
        {
          path: "/",
          name: "Home",
          component: Home,
        },
        {
          path: "/qrcode",
          name: "qrcode",
          component: () =>
            import(/* webpackChunkName: "qrcode" */ "./views/QRcode/Index.vue"),
        },
        {
          path: "/eth-converter",
          name: "EthConverter",
          component: () =>
            import(
              /* webpackChunkName: "EthConverter" */ "./views/eth-converter/index.vue"
            ),
        },
      ],
    },
    {
      path: "/profile",
      component: Profile,
      name: "Profile",
    },
    {
      path: "/mylove",
      component: () =>
        import(/* webpackChunkName: "my-love" */ "./views/my-love/Index.vue"),
      name: "MyLove",
    },
    {
      path: "/watch",
      component: Watch,
      name: "Watch",
    },
    {
      path: "/componentfunctional",
      component: ComponentFunctional,
      name: "ComponentFunctional",
    },
    {
      path: "/componentrender",
      component: ComponentRender,
      name: "ComponentRender",
    },
    {
      path: "/progressbar",
      component: ProgressBar,
      name: "ProgressBar",
    },
    {
      path: "/dcolorpicker",
      component: DColorPickerPage,
      name: "DColorPickerPage",
    },
    {
      path: "/daterangepicker",
      component: DateRangePicker,
      name: "DateRangePicker",
    },
    {
      path: "/console",
      component: Console,
      name: "Console",
    },
    {
      path: "/chat",
      component: Chat,
      name: "Chat",
    },

    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue"),
    },

    {
      path: "/form-jsx",
      name: "form-jsx",
      component: () =>
        import(/* webpackChunkName: "form-jsx" */ "./views/FormJSX/Index.vue"),
    },
    {
      path: "/clock",
      name: "clock",
      component: () =>
        import(/* webpackChunkName: "clock" */ "./views/clock/Index.vue"),
    },
  ],
});
