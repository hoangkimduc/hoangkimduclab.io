export function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * 
 charactersLength));
   }
   return result;
}

export function hexToRgb(hex) {
  // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
  var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shorthandRegex, function(m, r, g, b) {
    return r + r + g + g + b + b;
  });

  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null;
}

export function getTextColor(hexColor) {
    const rgbObj = hexToRgb(hexColor)
 

  // http://www.w3.org/TR/AERT#color-contrast
  const brightness = Math.round(((parseInt(rgbObj.r) * 299) +
                      (parseInt(rgbObj.g) * 587) +
                      (parseInt(rgbObj.b) * 114)) / 1000);
  const textColor = (brightness > 125) ? 'black' : 'white';
    return textColor;
}