import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    db: null,
    auth: null,
    user: null
  },
  mutations: {
    DB: (state, val) => (state.db = val),
    AUTH: (state, val) => (state.auth = val),
    USER: (state, val) => Vue.set(state, "user", val)
  },
  actions: {
    setUser({ commit }, val) {
      commit("USER", val);
      localStorage.setItem("user", JSON.stringify(val));
    }
  }
});
