// setup canvas
const canvas = document.querySelector("canvas");
const ctx = canvas.getContext("2d");

const width = (canvas.width = window.innerWidth);
const height = (canvas.height = window.innerHeight);

// function to generate random number

function random(min, max) {
  const num = Math.floor(Math.random() * (max - min + 1)) + min;
  return num;
}

class Ball {
  constructor(x, y, velX, velY, obj, size) {
    this.x = x;
    this.y = y;
    this.velX = velX || 1;
    this.velY = velY || 1;
    this.obj = obj;
    this.size = size;
  }

  draw() {
    ctx.beginPath();
    ctx.fillStyle = this.obj.color || "#fff";

    ctx.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
    ctx.fill();
    if (this.obj.loaded) {
      //   stretch;
      let imgX = this.x - this.size / 2;
      let imgY = this.y - this.size / 2;
      let sizeImg = this.size;
      if (this.obj.stretch) {
        imgX = this.x - this.size;
        imgY = this.y - this.size;
        sizeImg = this.size * 2;
      }
      ctx.shadowOffsetX = 10;
      ctx.shadowOffsetY = 10;
      ctx.shadowColor = "black";
      ctx.shadowBlur = 30;

      ctx.drawImage(this.obj.image, imgX, imgY, sizeImg, sizeImg);
    }
  }

  update() {
    if (this.x + this.size >= width) {
      this.velX = -this.velX;
    }

    if (this.x - this.size <= 0) {
      this.velX = -this.velX;
    }

    if (this.y + this.size >= height) {
      this.velY = -this.velY;
    }

    if (this.y - this.size <= 0) {
      this.velY = -this.velY;
    }

    this.x += this.velX;
    this.y += this.velY;
  }

  collisionDetect() {
    for (let j = 0; j < balls.length; j++) {
      if (!(this === balls[j])) {
        const dx = this.x - balls[j].x;
        const dy = this.y - balls[j].y;
        const distance = Math.sqrt(dx * dx + dy * dy);

        if (distance < this.size + balls[j].size) {
          balls[j].color = this.color =
            "rgb(" +
            random(0, 255) +
            "," +
            random(0, 255) +
            "," +
            random(0, 255) +
            ")";
        }
      }
    }
  }
}
var balls = [];
const techs = [
  {
    id: 1,
    name: "Vue.js",
    image: "/images/vue.png",
    color: "#fff",
  },
  {
    id: 2,
    name: "Vuetify.js",
    image: "/images/vuetify.png",
    color: "#fff",
  },
  {
    id: 3,
    name: "Nuxt.js",
    image: "/images/nuxt.png",
    color: "#fff",
  },
  {
    id: 4,
    name: "Flutter",
    image: "/images/flutter.png",
    color: "#fff",
  },
  {
    id: 5,
    name: "Laravel",
    image: "/images/laravel.svg",
    color: "rgb(255,45,32)",
  },
  {
    id: 6,
    name: "Chrome",
    image: "/images/chrome.png",
    stretch: true,
  },
  {
    id: 7,
    name: "Safari",
    image: "/images/safari.png",
    stretch: true,
  },
  {
    id: 8,
    name: "Firefox",
    image: "/images/firefox.png",
    stretch: true,
    color: "transparent",
  },
  {
    id: 9,
    name: "Edge",
    image: "/images/edge.png",
    stretch: true,
    color: "transparent",
  },
  {
    id: 10,
    name: "Firebase",
    image: "/images/firebase.png",
  },
  {
    id: 11,
    name: "JavaScript",
    image: "/images/js.png",
    stretch: true,
  },
];

balls = techs.map((item) => {
  const size = random(24, 35);
  const img = new Image();
  img.src = item.image;
  img.onload = function() {
    item.loaded = true;
  };
  item.image = img;

  return new Ball(
    random(size, width - size),
    random(size, height - size),
    random(-2, 2),
    random(-2, 2),
    item,
    size
  );
});

function loop() {
  ctx.fillStyle = "#fff";
  ctx.fillRect(0, 0, width, height);

  for (let i = 0; i < balls.length; i++) {
    balls[i].draw();
    balls[i].update();
    //  balls[i].collisionDetect();
  }

  requestAnimationFrame(loop);
}

loop();
