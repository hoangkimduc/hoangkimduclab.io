import Vue from "vue";
import vuetify from "@/vuetify"; // path to vuetify export
import "./assets/app.scss";
import App from "./App.vue";

import router from "./router";
import store from "./store";
//
import VueMoment from "vue-moment";
Vue.use(VueMoment);
//
import ATypography from "./components/ATypography";
Vue.component("a-typography", ATypography);

import VueCompositionAPI from "@vue/composition-api";
Vue.use(VueCompositionAPI);

Vue.config.productionTip = false;
//
let user = localStorage.getItem("user");
if (user) {
  store.commit("USER", JSON.parse(user));
}
//
new Vue({
  router,
  store,
  vuetify,

  render: (h) => h(App),
}).$mount("#app");
