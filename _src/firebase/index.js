import { initializeApp } from '@firebase/app'
import { getAuth } from "@firebase/auth";
import { getStorage } from '@firebase/storage'
import { getFirestore } from "@firebase/firestore";

// import '@firebase/firestore'
var config = {
    apiKey: 'AIzaSyBmtgIVLKso93GgXm40AktupqS5dNRZRaM',
    authDomain: 'hoangkimducgitlabio.firebaseapp.com',
    databaseURL: 'https://hoangkimducgitlabio.firebaseio.com',
    projectId: 'hoangkimducgitlabio',
    storageBucket: 'hoangkimducgitlabio.appspot.com',
    messagingSenderId: '787817743085',
    appId: '1:787817743085:web:ae1435365f605b1d9482e4',
    measurementId: 'G-TR51X5MDQL'
}
const app = initializeApp(config)
// firebaseApp.firestore().settings({ timestampsInSnapshots: true })
// firebase.analytics()



export const db = getFirestore(app)
export const auth = getAuth(app)
export const storage = getStorage(app);

