import DDialog from "./DDialog";
var DialogPlugin = {};
DialogPlugin.install = function(Vue) {
  Vue.component("DDialog", DDialog);
};

export default DialogPlugin;
