import { db, auth } from "@/utils/firebase";

export default async ({ store, redirect }) => {
 
  await new Promise((resolve, reject) => {
    const sub = auth.onAuthStateChanged(user => {
      console.log("auth:onAuthStateChanged", user);
      if (user) {
        store.dispatch("setUser", JSON.parse(JSON.stringify(user)));
      }
      resolve();
    });
    sub();
  });
};
