export const state = () => ({
  db: null,
  auth: null,
  user: null
});

export const getters = {};

export const mutations = {
  DB(state, val) {
    state.db = val;
  },
  AUTH(state, val) {
    state.auth = val;
  },
  USER(state, val) {
    state.user =  val;
  }
};

export const actions = {
  setUser({ commit }, val) {
    commit("USER", val);
  }
};
